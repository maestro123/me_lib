package com.msoft.android.mplayer.lib;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import com.msoft.android.mplayer.lib.models.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MediaStoreHelper {

    private static ContentValues sContentValues = null;

    public static class AlbumAZComparator implements Comparator<Album> {

        @Override
        public int compare(Album album, Album album2) {
            return album.Title.compareTo(album2.Title);
        }
    }

    public static ArrayList<Song> getSongs(Context ctx, Long id) {
        Cursor c = getSongsCursor(ctx, id);
        if (c == null || c.getCount() == 0) return null;
        c.moveToFirst();
        return getSongsList(c);
    }

    public static Cursor getSongsCursor(Context ctx, Long id) {
        String[] projection = {MediaStore.Audio.Media._ID, MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DURATION, MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.TRACK, MediaStore.Audio.Media.ALBUM};
        String selection = null;
        if (id != null) {
            selection = MediaStore.Audio.Media.ALBUM_ID + " = " + id;
        }
        return ctx.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection, selection, null, selection != null
                        ? MediaStore.Audio.Albums.DEFAULT_SORT_ORDER : MediaStore.Audio.Media.DEFAULT_SORT_ORDER);
    }

    public static ArrayList<Album> getAlbums(Context ctx, Long id) {
        Cursor c = getAlbumCursor(ctx, id);
        if (c == null || c.getCount() == 0) return null;
        c.moveToFirst();
        ArrayList<Album> albums = new ArrayList<Album>(c.getCount());
        do {
            Album a = new Album();
            a.Id = c.getLong(c.getColumnIndexOrThrow(MediaStore.Audio.Albums._ID));
            a.Title = c.getString(c.getColumnIndexOrThrow(MediaStore.Audio.Albums.ALBUM));
            a.author = c.getString(c.getColumnIndexOrThrow(MediaStore.Audio.Albums.ARTIST));
            a.artPath = c.getString(c.getColumnIndexOrThrow(MediaStore.Audio.Albums.ALBUM_ART));
            a.count = c.getInt(c.getColumnIndexOrThrow(MediaStore.Audio.Albums.NUMBER_OF_SONGS));
            a.FirstYear = c.getString(c.getColumnIndexOrThrow(MediaStore.Audio.Albums.FIRST_YEAR));
            a.LastYear = c.getString(c.getColumnIndexOrThrow(MediaStore.Audio.Albums.LAST_YEAR));
            albums.add(a);
        } while (c.moveToNext());
        c.close();
        return albums;
    }

    public static Album getAlbum(Context context, Long id) {
        ArrayList<Album> albums = getAlbums(context, id);
        return albums != null && albums.size() > 0 ? albums.get(0) : null;
    }

    public static Cursor getAlbumCursor(Context ctx, Long id) {
        String[] projection = {MediaStore.Audio.Albums._ID, MediaStore.Audio.Albums.ALBUM,
                MediaStore.Audio.Albums.ARTIST, MediaStore.Audio.Albums.ALBUM_ART,
                MediaStore.Audio.Albums.NUMBER_OF_SONGS, MediaStore.Audio.Albums.FIRST_YEAR,
                MediaStore.Audio.Albums.LAST_YEAR};
        String selection = null;
        if (id != null) {
            selection = MediaStore.Audio.Albums._ID + " = " + id;
        }
        return ctx.getContentResolver().query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, projection, selection,
                null, MediaStore.Audio.Albums.DEFAULT_SORT_ORDER);
    }

    public static Cursor getAlbumCoversCursor(Context ctx, Long id) {
        String[] projection = {MediaStore.Audio.Albums._ID, MediaStore.Audio.Albums.ALBUM_ART};
        String selection = null;
        if (id != null) {
            selection = MediaStore.Audio.Albums._ID + " = " + id;
        }
        return ctx.getContentResolver().query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                projection, selection, null, MediaStore.Audio.Albums.DEFAULT_SORT_ORDER);
    }

    public static ArrayList<Genre> getGenres(Context ctx) {
        Cursor c = getGenresCursor(ctx);
        if (c == null || c.getCount() == 0) return null;
        ArrayList<Genre> genres = new ArrayList<Genre>(c.getCount());
        c.moveToFirst();
        do {
            Genre g = new Genre();
            g.Id = c.getLong(c.getColumnIndexOrThrow(MediaStore.Audio.Genres._ID));
            g.Title = c.getString(c.getColumnIndexOrThrow(MediaStore.Audio.Genres.NAME));
            List<Song> songs = getSongsByGenre(ctx, g.Id);
            g.Songs = songs != null ? songs.toArray(new Song[songs.size()]) : null;
            genres.add(g);
        } while (c.moveToNext());
        c.close();
        return genres;
    }

    public static ArrayList<Song> getSongsByGenre(Context ctx, Long id) {
        Uri query = MediaStore.Audio.Genres.Members.getContentUri("external", id);
        Cursor cursor = ctx.getContentResolver().query(query, null, null, null, MediaStore.Audio.Media.DEFAULT_SORT_ORDER);
        if (cursor.getCount() == 0) return null;
        cursor.moveToFirst();
        return getSongsList(cursor);
    }

    public static Cursor getGenresCursor(Context ctx) {
        String[] projection = {MediaStore.Audio.Genres._ID, MediaStore.Audio.Genres.NAME};
        return ctx.getContentResolver().query(MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI,
                projection, null, null, MediaStore.Audio.Genres.DEFAULT_SORT_ORDER);
    }

    public static ArrayList<Playlist> getPlaylists(Context ctx) {
        Cursor c = getPlaylistCursor(ctx);
        if (c == null || c.getCount() == 0) return null;
        c.moveToFirst();
        ArrayList<Playlist> playlists = new ArrayList<Playlist>(c.getCount());
        do {
            Playlist p = new Playlist();
            p.Id = c.getLong(c.getColumnIndexOrThrow(MediaStore.Audio.Playlists._ID));
            p.Title = c.getString(c.getColumnIndexOrThrow(MediaStore.Audio.Playlists.NAME));
            playlists.add(p);
        } while (c.moveToNext());
        c.close();
        return playlists;
    }

    public static ArrayList<Song> getSongsByPlaylist(Context ctx, Long id) {
        String[] projection = {MediaStore.Audio.Playlists.Members._ID, MediaStore.Audio.Playlists.Members.ALBUM_ID,
                MediaStore.Audio.Playlists.Members.TITLE, MediaStore.Audio.Playlists.Members.DATA,
                MediaStore.Audio.Playlists.Members.DURATION, MediaStore.Audio.Playlists.Members.ARTIST,
                MediaStore.Audio.Playlists.Members.PLAYLIST_ID};
        String selection = null;
        if (id == null) {
            selection = MediaStore.Audio.Playlists.Members.PLAYLIST_ID + " = " + id;
        }
        Cursor c = ctx.getContentResolver().query(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, projection, selection,
                null, MediaStore.Audio.Playlists.Members.DEFAULT_SORT_ORDER);
        if (c == null || c.getCount() == 0) return null;
        c.moveToFirst();
        return getSongsList(c);
    }

    public static Cursor getPlaylistCursor(Context ctx) {
        String[] projection = {MediaStore.Audio.Playlists._ID, MediaStore.Audio.Playlists.NAME};
        return ctx.getContentResolver().query(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, projection, null,
                null, MediaStore.Audio.Playlists.DEFAULT_SORT_ORDER);
    }

    public static boolean createPlayList(Context context, String name) {
        ContentValues values = new ContentValues(1);
        values.put(MediaStore.Audio.Playlists.NAME, name);
        return context.getContentResolver().insert(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, values) != null;
    }

    private static ArrayList<Song> getSongsList(Cursor c) {
        ArrayList<Song> songs = new ArrayList<Song>(c.getCount());
        do {
            Song s = new Song();
            s.Id = c.getLong(c.getColumnIndexOrThrow(MediaStore.Audio.Media._ID));
            s.ParentId = c.getLong(c.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM_ID));
            s.Title = c.getString(c.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE));
            s.Path = c.getString(c.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA));
            s.Duration = c.getLong(c.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION));
            s.Author = c.getString(c.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST));
            s.Album = c.getString(c.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM));
            s.TrackNumberCD = String.valueOf(c.getInt(c.getColumnIndexOrThrow(MediaStore.Audio.Media.TRACK)));
            songs.add(s);
        } while (c.moveToNext());
        c.close();
        return songs;
    }

    public static Cursor getArtistsCursor(Context ctx) {
        String[] projection = {MediaStore.Audio.Artists._ID, MediaStore.Audio.Artists.ARTIST,
                MediaStore.Audio.Artists.NUMBER_OF_ALBUMS};
        return ctx.getContentResolver().query(MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI, projection, null,
                null, MediaStore.Audio.Artists.DEFAULT_SORT_ORDER);
    }

    public static ArrayList<Artist> getArtists(Context ctx) {
        Cursor cursor = getArtistsCursor(ctx);
        if (cursor.getCount() == 0) return null;
        cursor.moveToFirst();
        ArrayList<Artist> artists = new ArrayList<Artist>(cursor.getCount());
        do {
            Artist artist = new Artist();
            artist.Id = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Artists._ID));
            artist.Title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Artists.ARTIST));
            artist.Count = cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Artists.NUMBER_OF_ALBUMS));
            artists.add(artist);
        } while (cursor.moveToNext());
        cursor.close();
        return artists;
    }

    public static ArrayList<Song> getSongsByArtist(Context ctx, Long id) {
        String[] projection = {MediaStore.Audio.Media._ID, MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DURATION, MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ARTIST_ID, MediaStore.Audio.Media.TRACK,
                MediaStore.Audio.Media.ALBUM};
        String selection = null;
        if (id != null) {
            selection = MediaStore.Audio.Media.ARTIST_ID + " = " + id;
        }
        Cursor cursor = ctx.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection, selection, null, MediaStore.Audio.Media.DEFAULT_SORT_ORDER);
        if (cursor.getCount() == 0) return null;
        cursor.moveToFirst();
        return getSongsList(cursor);
    }


    //ADDITIONAL

    public static List<Image> getImages(Context context, Long parentId) {
        Cursor c = getImagesCursor(context, parentId);
        if (c == null || c.getCount() == 0) return null;
        ArrayList<Image> images = new ArrayList<Image>(c.getCount());
        c.moveToFirst();
        do {
            Image image = new Image();
            image.Title = c.getString(c.getColumnIndexOrThrow(MediaStore.Images.Media.TITLE));
            image.Path = c.getString(c.getColumnIndexOrThrow(MediaStore.Images.Media.DATA));
            image.ParentId = c.getInt(c.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_ID));
            images.add(image);
        } while (c.moveToNext());
        c.close();
        return images;
    }

    public static Cursor getImagesCursor(Context context, Long parentId) {
        String[] projection = {MediaStore.Images.Media._ID, MediaStore.Images.Media.TITLE,
                MediaStore.Images.Media.DATA, MediaStore.Images.Media.BUCKET_ID,};
        String selection = null;
        if (parentId != null) {
            selection = MediaStore.Images.Media.BUCKET_ID + " = " + parentId;
        }
        return context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection, selection, null, MediaStore.Images.Media.DEFAULT_SORT_ORDER);
    }

    public static ArrayList<MediaBucket> getImageBuckets(Context context, boolean collectItems) {
        String[] projection = {MediaStore.Images.Media.BUCKET_ID, MediaStore.Images.Media.BUCKET_DISPLAY_NAME};
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection, null, null, MediaStore.Images.Media.DEFAULT_SORT_ORDER);
        if (cursor.getCount() == 0) return null;
        ArrayList<String> added = new ArrayList<String>();
        ArrayList<MediaBucket> out = new ArrayList<MediaBucket>();
        cursor.moveToFirst();
        do {
            String title = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME));
            if (!added.contains(title)) {
                MediaBucket bucket = new MediaBucket();
                bucket.Title = title;
                bucket.Id = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_ID));
                List<Image> images = getImages(context, bucket.Id);
                if (images != null && images.size() > 0) {
                    bucket.setChilds(images.toArray(new Image[images.size()]));
                }
                out.add(bucket);
                added.add(title);
            }
        } while (cursor.moveToNext());
        return out;
    }

    public static ArrayList<Video> getVideos(Context context, Long parentId) {
        Cursor c = getVideosCursor(context, parentId);
        if (c == null || c.getCount() == 0) return null;
        ArrayList<Video> images = new ArrayList<Video>(c.getCount());
        c.moveToFirst();
        do {
            Video video = new Video();
            video.Title = c.getString(c.getColumnIndexOrThrow(MediaStore.Video.Media.TITLE));
            video.Path = c.getString(c.getColumnIndexOrThrow(MediaStore.Video.Media.DATA));
            video.ParentId = c.getInt(c.getColumnIndexOrThrow(MediaStore.Video.Media.BUCKET_ID));
            video.Duration = c.getLong(c.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION));
            images.add(video);
        } while (c.moveToNext());
        return images;
    }

    public static Cursor getVideosCursor(Context context, Long parentId) {
        String[] projection = {MediaStore.Video.Media._ID, MediaStore.Video.Media.TITLE,
                MediaStore.Video.Media.DATA, MediaStore.Video.Media.BUCKET_ID, MediaStore.Video.Media.DURATION};
        String selection = null;
        if (parentId != null) {
            selection = MediaStore.Video.Media.BUCKET_ID + " = " + parentId;
        }
        return context.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                projection, selection, null, MediaStore.Video.Media.DEFAULT_SORT_ORDER);
    }

    public static List<MediaBucket> getVideosBuckets(Context context) {
        String[] projection = {MediaStore.Video.Media.BUCKET_ID, MediaStore.Video.Media.BUCKET_DISPLAY_NAME};
        Cursor cursor = context.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                projection, null, null, MediaStore.Video.Media.DEFAULT_SORT_ORDER);
        if (cursor.getCount() == 0) return null;
        ArrayList<String> added = new ArrayList<String>();
        ArrayList<MediaBucket> out = new ArrayList<MediaBucket>();
        cursor.moveToFirst();
        do {
            String title = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.BUCKET_DISPLAY_NAME));
            if (!added.contains(title)) {
                MediaBucket bucket = new MediaBucket();
                bucket.Title = title;
                bucket.Id = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.BUCKET_ID));
                out.add(bucket);
                added.add(title);
            }
        } while (cursor.moveToNext());
        return out;
    }

    public static ArrayList<Album> searchAlbum(Context context, String query) {
        query = query.toLowerCase();
        List<Album> albums = getAlbums(context, null);
        ArrayList<Album> match = new ArrayList<Album>();
        List<Album> other = new ArrayList<Album>();
        if (albums != null) {
            for (Album album : albums) {
                if (album.Title.toLowerCase().contains(query)) {
                    if (album.Title.toLowerCase().startsWith(query)) {
                        match.add(album);
                    } else {
                        other.add(album);
                    }
                }
            }
            SearchComparator comparator = new SearchComparator();
            Collections.sort(match, comparator);
            Collections.sort(other, comparator);
        }
        return match;
    }

    public static List<Artist> searchArtist(Context context, String query) {
        query = query.toLowerCase();
        List<Artist> artists = getArtists(context);
        List<Artist> match = new ArrayList<Artist>();
        List<Artist> other = new ArrayList<Artist>();
        if (artists != null) {
            for (Artist artist : artists) {
                if (artist.Title.toLowerCase().contains(query)) {
                    if (artist.Title.toLowerCase().startsWith(query)) {
                        match.add(artist);
                    } else {
                        other.add(artist);
                    }
                }
            }
            SearchComparator comparator = new SearchComparator();
            Collections.sort(match, comparator);
            Collections.sort(other, comparator);
        }
        return match;
    }

    public static List<Song> searchSongs(Context context, String query) {
        query = query.toLowerCase();
        List<Song> songs = getSongs(context, null);
        List<Song> match = new ArrayList<Song>();
        List<Song> other = new ArrayList<Song>();
        if (songs != null) {
            for (Song song : songs) {
                if (song.Title.toLowerCase().contains(query)) {
                    if (song.Title.toLowerCase().startsWith(query)) {
                        match.add(song);
                    } else {
                        other.add(song);
                    }
                }
            }
            SearchComparator comparator = new SearchComparator();
            Collections.sort(match, comparator);
            Collections.sort(other, comparator);
        }
        return match;
    }

    public static List<Genre> searchGenre(Context context, String query) {
        query = query.toLowerCase();
        List<Genre> genres = getGenres(context);
        List<Genre> match = new ArrayList<Genre>();
        List<Genre> other = new ArrayList<Genre>();
        if (genres != null) {
            for (Genre genre : genres) {
                if (genre.Title.toLowerCase().contains(query)) {
                    if (genre.Title.toLowerCase().startsWith(query)) {
                        match.add(genre);
                    } else {
                        other.add(genre);
                    }
                }
            }
            SearchComparator comparator = new SearchComparator();
            Collections.sort(match, comparator);
            Collections.sort(other, comparator);
        }
        return match;
    }

    public static class SearchComparator implements Comparator<BaseMediaItem> {
        @Override
        public int compare(BaseMediaItem item, BaseMediaItem item2) {
            return item.Title.toLowerCase().compareTo(item2.Title.toLowerCase());
        }
    }


}
