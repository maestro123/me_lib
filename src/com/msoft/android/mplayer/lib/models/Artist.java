package com.msoft.android.mplayer.lib.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by artyom on 7/23/14.
 */
public class Artist extends BaseMediaItem {

    public int Count;

    public Artist() {
    }

    public Artist(Parcel source) {
        readFromParcel(source);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(Count);
    }

    @Override
    public void readFromParcel(Parcel source) {
        super.readFromParcel(source);
        Count = source.readInt();
    }

    public String getCount() {
        return new StringBuilder().append(Count).toString();
    }

    public static Parcelable.Creator<Artist> CREATOR = new Creator<Artist>() {

        @Override
        public Artist[] newArray(int size) {
            return new Artist[size];
        }

        @Override
        public Artist createFromParcel(Parcel source) {
            return new Artist(source);
        }
    };
}
