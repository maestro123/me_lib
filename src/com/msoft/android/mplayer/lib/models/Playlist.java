package com.msoft.android.mplayer.lib.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Playlist extends BaseMediaItem {

    public int count;

    public Playlist() {
    }

    public Playlist(Parcel source) {
        readFromParcel(source);
    }

    public String getCount() {
        return String.valueOf(count);
    }

    @Override
    public void readFromParcel(Parcel source) {
        super.readFromParcel(source);
        count = source.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(count);
    }

    public static Parcelable.Creator<Playlist> CREATOR = new Creator<Playlist>() {

        @Override
        public Playlist[] newArray(int size) {
            return new Playlist[size];
        }

        @Override
        public Playlist createFromParcel(Parcel source) {
            return new Playlist(source);
        }
    };

}
