package com.msoft.android.mplayer.lib.models;

import android.os.Parcel;
import android.os.Parcelable;

public class BaseMediaItem implements Parcelable{

    public long Id;
    public long ParentId;
    public String Title;
    public String Path;

    public BaseMediaItem() {
    }

    public BaseMediaItem(Parcel source) {
        readFromParcel(source);
    }

    public void readFromParcel(Parcel source) {
        Id = source.readLong();
        ParentId = source.readLong();
        Title = source.readString();
        Path = source.readString();
    }

    public void readChildValue(Parcel source) {

    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(Id);
        dest.writeLong(ParentId);
        dest.writeString(Title);
        dest.writeString(Path);
    }

    public void writeChildValues() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        return this == o;
    }
}
