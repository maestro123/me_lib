package com.msoft.android.mplayer.lib.models;

import android.os.Parcel;

/**
 * Created by artyom on 9/1/14.
 */
public class MediaBucket extends BaseMediaItem {

    public BaseMediaItem[] childs;
    public TYPE type;

    enum TYPE {
        VIDEO, IMAGE
    }

    public MediaBucket() {
    }

    public MediaBucket(Parcel source) {
        readFromParcel(source);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof String) {
            return Title.equals(o);
        } else if (o instanceof MediaBucket)
            return Title.equals(((MediaBucket) o).Title);
        return super.equals(o);
    }

    public int getChildCount() {
        return childs != null ? childs.length : 0;
    }

    public boolean haveChilds() {
        return childs != null && childs.length > 0;
    }

    public BaseMediaItem[] getChilds() {
        return childs;
    }

    public void setChilds(BaseMediaItem[] items) {
        childs = items;
    }

    public static final Creator<MediaBucket> CREATOR = new Creator<MediaBucket>() {
        @Override
        public MediaBucket createFromParcel(Parcel parcel) {
            return new MediaBucket(parcel);
        }

        @Override
        public MediaBucket[] newArray(int i) {
            return new MediaBucket[i];
        }
    };

}
