package com.msoft.android.mplayer.lib.models;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;

public class Song extends BaseMediaItem {

    public long Duration;
    public String Author;
    public String Album;
    public String AlbumArtist;
    public String Artist;
    public String Bitrate;
    public String TrackNumberCD;
    public String Compilation;
    public String Composer;
    public String Date;
    public String Year;
    public String DiscNumber;
    public String Genre;
    public String MimeType;
    public String Writer;

    public Song() {
    }

    public Song(Parcel source) {
        readFromParcel(source);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeLong(Duration);
        dest.writeString(Author);
        dest.writeString(Album);
        dest.writeString(AlbumArtist);
        dest.writeString(Artist);
        dest.writeString(Bitrate);
        dest.writeString(TrackNumberCD);
        dest.writeString(Compilation);
        dest.writeString(Composer);
        dest.writeString(Date);
        dest.writeString(Year);
        dest.writeString(DiscNumber);
        dest.writeString(Genre);
        dest.writeString(MimeType);
        dest.writeString(Writer);
    }

    @Override
    public void readFromParcel(Parcel source) {
        super.readFromParcel(source);
        Duration = source.readLong();
        Author = source.readString();
        Album = source.readString();
        AlbumArtist = source.readString();
        Artist = source.readString();
        Bitrate = source.readString();
        TrackNumberCD = source.readString();
        Compilation = source.readString();
        Composer = source.readString();
        Date = source.readString();
        Year = source.readString();
        DiscNumber = source.readString();
        Genre = source.readString();
        MimeType = source.readString();
        Writer = source.readString();
    }

    public String dump() {
        return "id = " + Id + " \n" + "parent id" + ParentId + " \n " +
                "title" + Title + " \n " + "path = " + Path + " \n " + "duration = " + Duration;
    }

    public static Parcelable.Creator<Song> CREATOR = new Creator<Song>() {

        @Override
        public Song[] newArray(int size) {
            return new Song[size];
        }

        @Override
        public Song createFromParcel(Parcel source) {
            return new Song(source);
        }
    };

    @Override
    public boolean equals(Object o) {
        if (o instanceof Song) {
            Song s = (Song) o;
            return s.Path.equals(Path);
        }
        return super.equals(o);
    }

    @Override
    public String toString() {
        return "Song: " + Title + ", " + Author + ", " + Duration;
    }
}
