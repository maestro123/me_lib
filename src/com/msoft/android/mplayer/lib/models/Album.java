package com.msoft.android.mplayer.lib.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Album extends BaseMediaItem {

    public String artPath;
    public int count;
    public String author;
    public String FirstYear;
    public String LastYear;

    public Album() {

    }

    public Album(Parcel source) {
        readFromParcel(source);
    }

    public String getCount() {
        return String.valueOf(count);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(artPath);
        dest.writeInt(count);
        dest.writeString(author);
    }

    @Override
    public void readFromParcel(Parcel source) {
        super.readFromParcel(source);
        artPath = source.readString();
        count = source.readInt();
        author = source.readString();
    }

    public static Parcelable.Creator<Album> CREATOR = new Creator<Album>() {

        @Override
        public Album[] newArray(int size) {
            return new Album[size];
        }

        @Override
        public Album createFromParcel(Parcel source) {
            return new Album(source);
        }
    };
}
