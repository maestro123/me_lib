package com.msoft.android.mplayer.lib.models;

import android.os.Parcel;

/**
 * Created by artyom on 9/1/14.
 */
public class Image extends BaseMediaItem {

    public Image() {
    }

    public Image(Parcel source) {
        readFromParcel(source);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    @Override
    public void readFromParcel(Parcel source) {
        super.readFromParcel(source);
    }

    public static final Creator<Image> CREATOR = new Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel parcel) {
            return null;
        }

        @Override
        public Image[] newArray(int i) {
            return new Image[i];
        }
    };

}
