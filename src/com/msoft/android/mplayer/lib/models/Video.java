package com.msoft.android.mplayer.lib.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by artyom on 9/1/14.
 */
public class Video extends BaseMediaItem {

    public long Duration;

    public Video() {
    }

    public Video(Parcel source) {
        readFromParcel(source);
    }

    public static Parcelable.Creator<Video> CREATOR = new Creator<Video>() {

        @Override
        public Video[] newArray(int size) {
            return new Video[size];
        }

        @Override
        public Video createFromParcel(Parcel source) {
            return new Video(source);
        }
    };

}
