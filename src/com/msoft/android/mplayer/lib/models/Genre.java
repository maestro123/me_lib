package com.msoft.android.mplayer.lib.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Genre extends BaseMediaItem {

    public Song[] Songs;

    public Genre() {
    }

    public Genre(Parcel source) {
        readFromParcel(source);
    }

    public String getCount() {
        return String.valueOf(Songs != null ? Songs.length : 0);
    }

    public int getCountInt() {
        return Songs != null ? Songs.length : 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelableArray(Songs, 0);
    }

    @Override
    public void readFromParcel(Parcel source) {
        super.readFromParcel(source);
        Songs = (Song[]) source.readParcelableArray(Song.class.getClassLoader());
    }

    public static Parcelable.Creator<Genre> CREATOR = new Creator<Genre>() {

        @Override
        public Genre[] newArray(int size) {
            return new Genre[size];
        }

        @Override
        public Genre createFromParcel(Parcel source) {
            return new Genre(source);
        }
    };

}
