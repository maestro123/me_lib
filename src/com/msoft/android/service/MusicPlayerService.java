package com.msoft.android.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

/**
 * Created by Artyom on 06.08.2014.
 */
public class MusicPlayerService extends Service {

    private LocalBinder mBinder = new LocalBinder();
    private MusicPlayerEngine mEngine = MusicPlayerEngine.getInstance();

    public class LocalBinder extends Binder {
        public MusicPlayerService getService() {
            return MusicPlayerService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (!mEngine.isInitialized())
            mEngine.initialize(getApplicationContext());
    }
}
