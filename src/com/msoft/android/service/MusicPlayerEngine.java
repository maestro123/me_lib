package com.msoft.android.service;

import android.content.*;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import com.msoft.android.mplayer.lib.models.Song;

import java.io.IOException;
import java.util.*;

/**
 * Created by Artyom on 04.08.2014.
 */
public class MusicPlayerEngine extends BroadcastReceiver implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {

    public static final String TAG = MusicPlayerEngine.class.getSimpleName();

    private static volatile MusicPlayerEngine instance;

    public static synchronized MusicPlayerEngine getInstance() {
        MusicPlayerEngine localInstance = instance;
        if (localInstance == null) {
            synchronized (MusicPlayerEngine.class) {
                if (localInstance == null) {
                    localInstance = instance = new MusicPlayerEngine();
                }
            }
        }
        return localInstance;
    }

    public enum MUSIC_PLAYER_EVENT {
        START, RESUME, PAUSE, END, ERROR, TIME_UPDATE, SONGS_CHANGE
    }

    public interface MusicPlayerListener {
        public void onMusicPlayerEvent(MUSIC_PLAYER_EVENT event);
    }

    public static final String PACKAGE = "maestro.music";
    public static final String ACTION_PLAY_START = PACKAGE + ".PLAY_START";
    public static final String ACTION_PLAY_RESUME = PACKAGE + ".PLAY_RESUME";
    public static final String ACTION_PLAY_PAUSE = PACKAGE + "PLAY_PAUSE";
    public static final String ACTION_PLAY_END = PACKAGE + ".PLAY_END";
    public static final String ACTION_SONG_LIST_CHANGE = PACKAGE + ".SONG_LIST_CHANGE";
    public static final String ACTION_PLAY_ERROR = PACKAGE + ".PLAY_ERROR";

    private static final String PARAM_LOOP = "param_loop";
    private static final String PARAM_SHUFFLE = "param_shuffle";

    private static final String PARAM_PREVIOUS_SONGS = "param_previous_songs";
    private static final String PARAM_PREVIOUS_SONG = "param_previous_song";

    private static final long TIMER_UPDATE_TIME = 1000;
    private static final int MSG_NOTIFY = 0;

    private IntentFilter mIntentFilter = new IntentFilter();

    {
        mIntentFilter.addAction(Intent.ACTION_HEADSET_PLUG);
        mIntentFilter.addAction(AudioManager.RINGER_MODE_CHANGED_ACTION);
        mIntentFilter.addAction(Intent.ACTION_MEDIA_BUTTON);
    }

    private Context mContext;
    private MediaPlayer mediaPlayer;
    private Song[] mCurrentPlaySongs = new Song[0];
    private Song[] mOriginalPlaySongs = new Song[0];
    private Song[] mShuffledPlaySongs = new Song[0];
    private Song mCurrentPlaySong;
    private LOOP_STATE mCurrentLoop;
    private HashMap<Object, MusicPlayerListener> mListeners = new HashMap<Object, MusicPlayerListener>();
    private Timer mTimer = new Timer();
    private int mCurrentPlaySongPosition;
    private boolean isShuffle, isReady;

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_NOTIFY:
                    final MUSIC_PLAYER_EVENT event = (MUSIC_PLAYER_EVENT) msg.obj;
                    ArrayList<MusicPlayerListener> listeners = new ArrayList<MusicPlayerListener>(mListeners.values());
                    for (MusicPlayerListener listener : listeners) {
                        listener.onMusicPlayerEvent(event);
                    }
                    break;
            }
        }
    };

    private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {

        private boolean isPlayingBeforeStateChange;

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);
            isPlayingBeforeStateChange = isPlaying();
            pause();
        }
    };

    private class PostRunnable implements Runnable {

        private MUSIC_PLAYER_EVENT event;

        public PostRunnable(MUSIC_PLAYER_EVENT event) {
            this.event = event;
        }

        @Override
        public void run() {
            ArrayList<MusicPlayerListener> listeners = new ArrayList<MusicPlayerListener>(mListeners.values());
            for (MusicPlayerListener listener : listeners) {
                listener.onMusicPlayerEvent(event);
            }
        }
    }

    public boolean isInitialized() {
        return mContext != null;
    }

    public void initialize(Context context) {
        mContext = context;
        mContext.registerReceiver(this, mIntentFilter);

        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);

        AudioManager mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.registerMediaButtonEventReceiver(new ComponentName(context.getPackageName(), MusicPlayerEngine.class.getName()));

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnErrorListener(this);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setOnPreparedListener(this);

        mCurrentLoop = getLoop();
        isShuffle = getShuffle();
        if (!canPlay()) {
            restoreSongs();
        }
    }

    public void onDestroy() {
        if (mContext != null) {
            if (canPlay())
                storeSongs();
            mContext.unregisterReceiver(this);
        }
    }

    public Song getCurrentPlayingSong() {
        return mCurrentPlaySongPosition < mCurrentPlaySongs.length ? mCurrentPlaySongs[mCurrentPlaySongPosition] : null;
    }

    public int getCurrentPlayingSongPosition() {
        return mCurrentPlaySongPosition;
    }

    public Song[] getCurrentPlayingSongs() {
        return mCurrentPlaySongs;
    }

    public void play(Song[] songs, int position) {
        mOriginalPlaySongs = Arrays.copyOf(songs, songs.length);
        mShuffledPlaySongs = Arrays.copyOf(songs, songs.length);
        mCurrentPlaySongPosition = position;
        mCurrentPlaySongs = mOriginalPlaySongs;
        mCurrentPlaySong = mCurrentPlaySongs[position];
        if (isShuffle)
            shuffleList();
        Log.e(TAG, "should play = " + position + ", current song = " + mCurrentPlaySong);
        notifyHandler(MUSIC_PLAYER_EVENT.SONGS_CHANGE);
        play();
//        storeSongs();
    }

    public void play(int position) {
        stop();
        mCurrentPlaySongPosition = position;
        play();
    }

    public void play() {
        synchronized (mediaPlayer) {
            if (canPlay()) {
                mCurrentPlaySong = mCurrentPlaySongs[mCurrentPlaySongPosition];
                try {
                    uiHandler.removeMessages(MSG_NOTIFY);
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.seekTo(0);
                        mediaPlayer.pause();
                    }
                    mediaPlayer.reset();
                    mediaPlayer.setDataSource(mCurrentPlaySong.Path);
                    mediaPlayer.prepareAsync();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mediaPlayer.start();
        isReady = true;
        mTimer = new Timer();
        mTimer.schedule(new TimeTimerTask(), TIMER_UPDATE_TIME);
        sendBroadcast(ACTION_PLAY_START);
        notifyHandler(MUSIC_PLAYER_EVENT.START);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mediaPlayer.reset();
        ensureCompletion();
    }

    public void resume() {
        mediaPlayer.start();
        sendBroadcast(ACTION_PLAY_RESUME);
        notifyHandler(MUSIC_PLAYER_EVENT.RESUME);
        mTimer = new Timer();
        mTimer.schedule(new TimeTimerTask(), TIMER_UPDATE_TIME);
    }

    public void pause() {
        mediaPlayer.pause();
        sendBroadcast(ACTION_PLAY_PAUSE);
        notifyHandler(MUSIC_PLAYER_EVENT.PAUSE);
        mTimer.cancel();
    }

    public void stop() {
        mediaPlayer.stop();
        sendBroadcast(ACTION_PLAY_END);
        notifyHandler(MUSIC_PLAYER_EVENT.END);
        mTimer.cancel();
    }

    public void next() {
        if (canPlay())
            if (mCurrentPlaySongPosition != mCurrentPlaySongs.length - 1) {
                mCurrentPlaySongPosition++;
                play();
            }
    }

    public void prev() {
        if (canPlay())
            if (mediaPlayer.getCurrentPosition() > 1000 * 5) {
                mediaPlayer.seekTo(0);
            } else if (mCurrentPlaySongPosition > 0) {
                mCurrentPlaySongPosition--;
                play();
            }
    }

    public void setCurrentPlayingPosition(int position) {
        mCurrentPlaySongPosition = position;
        play();
    }

    public void seekTo(int progress) {
        mediaPlayer.seekTo(progress);
    }

    public boolean canPlay() {
        return isInitialized() && mCurrentPlaySongs != null && mCurrentPlaySongs.length > 0;
    }

    public boolean isCurrentPlayingSong(Song song) {
        return canPlay() && mCurrentPlaySongs[mCurrentPlaySongPosition].equals(song);
    }

    public boolean isPlaying() {
        return mediaPlayer.isPlaying();
    }

    public int getCurrentPlayingPosition() {
        return isReady ? mediaPlayer.getCurrentPosition() : 0;
    }

    public int getDuration() {
        return isReady ? mediaPlayer.getDuration() : 1;
    }

    public void notifyHandler(MUSIC_PLAYER_EVENT event) {
        uiHandler.obtainMessage(MSG_NOTIFY, event).sendToTarget();
    }

    private boolean ensureCompletion() {
        boolean hasNextSong = mCurrentPlaySongPosition != mCurrentPlaySongs.length - 1;
        switch (mCurrentLoop) {
            case FULL_LOOP:
                if (!hasNextSong) {
                    mCurrentPlaySongPosition = 0;
                    play();
                    return true;
                }
            case NO_LOOP:
                if (hasNextSong) {
                    mCurrentPlaySongPosition++;
                    play();
                    return true;
                }
                return false;
            case SINGLE_LOOP:
                play();
                return true;
        }
        return false;
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    public void sendBroadcast(String action) {
        Intent intent = new Intent(action);
        mContext.sendBroadcast(intent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
            int state = intent.getIntExtra("state", -1);
            switch (state) {
                case 0:
                    Log.d(TAG, "Headset is unplugged");
                    pause();
                    break;
                case 1:
                    Log.d(TAG, "Headset is plugged");
                    resume();
                    break;
                default:
                    Log.d(TAG, "I have no idea what the headset state is");
                    pause();
                    break;
            }
        } else if (intent.getAction().equals(AudioManager.RINGER_MODE_CHANGED_ACTION)) {
            Log.e(TAG, "ringer mode change");
        } else if (intent.getAction().equals(Intent.ACTION_MEDIA_BUTTON)) {
            KeyEvent event = intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
            if (event == null) {
                return;
            }
        }
    }

    public void setLoop(LOOP_STATE loopState) {
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putInt(PARAM_LOOP, loopState.ordinal()).commit();
        mCurrentLoop = loopState;
    }

    public LOOP_STATE getLoop() {
        return LOOP_STATE.values()[PreferenceManager.getDefaultSharedPreferences(mContext).getInt(PARAM_LOOP, 2)];
    }

    public void setShuffle(boolean shuffle) {
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putBoolean(PARAM_SHUFFLE, shuffle).commit();
        isShuffle = shuffle;
        if (isShuffle) {
            shuffleList();
        } else {
            normalizeList();
        }
    }

    public boolean getShuffle() {
        return PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean(PARAM_SHUFFLE, false);
    }

    private void shuffleList() {
        if (canPlay()) {
            Song temp;
            int index;
            Random random = new Random();
            for (int i = mShuffledPlaySongs.length - 1; i > 0; i--) {
                index = random.nextInt(i + 1);
                temp = mShuffledPlaySongs[index];
                mShuffledPlaySongs[index] = mShuffledPlaySongs[i];
                mShuffledPlaySongs[i] = temp;
            }
            mCurrentPlaySongs = mShuffledPlaySongs;
            ensureCurrentPosition();
            sendBroadcast(ACTION_SONG_LIST_CHANGE);
            notifyHandler(MUSIC_PLAYER_EVENT.SONGS_CHANGE);
        }
    }

    private void normalizeList() {
        if (canPlay()) {
            mCurrentPlaySongs = mOriginalPlaySongs;
            ensureCurrentPosition();
            sendBroadcast(ACTION_SONG_LIST_CHANGE);
            notifyHandler(MUSIC_PLAYER_EVENT.SONGS_CHANGE);
        }
    }

    private void ensureCurrentPosition() {
        Log.e(TAG, "current position = " + mCurrentPlaySongPosition + ", current song = " + mCurrentPlaySong);
        final int size = mCurrentPlaySongs.length;
        for (int i = 0; i < size; i++) {
            if (mCurrentPlaySongs[i] == mCurrentPlaySong) {
                mCurrentPlaySongPosition = i;
                Log.e(TAG, "out position = " + i + ", song = " + mCurrentPlaySongs[i]);
                break;
            }
        }
    }

    public enum LOOP_STATE {
        NO_LOOP, SINGLE_LOOP, FULL_LOOP
    }

    public void attachListener(Object key, MusicPlayerListener listener) {
        if (mListeners.containsKey(key))
            mListeners.remove(key);
        mListeners.put(key, listener);
    }

    public void detachListener(Object key) {
        mListeners.remove(key);
    }

    private void storeSongs() {
//        ArrayList<String> set = new ArrayList<String>();
//        for (Song song : mCurrentPlaySongs) {
//            set.add(song.Path);
//        }
//        PreferenceManager.getDefaultSharedPreferences(mContext).edit()
//                .putStringSet(PARAM_PREVIOUS_SONGS, new HashSet<String>(set));
//        PreferenceManager.getDefaultSharedPreferences(mContext).edit()
//                .putString(PARAM_PREVIOUS_SONG, getCurrentPlayingSong().Path).commit();
    }

    private void restoreSongs() {
//        HashSet<String> set = new HashSet<String>();
//        PreferenceManager.getDefaultSharedPreferences(mContext).getStringSet(PARAM_PREVIOUS_SONGS, set);
//        List<Song> allSongs = MediaStoreHelper.getSongs(mContext, null);
//        ArrayList<Song> out = new ArrayList<Song>();
//        for (Song song : allSongs) {
//            Iterator<String> itr = set.iterator();
//            while (itr.hasNext()) {
//                String path = itr.next();
//                if (song.Path.equals(path)) {
//                    out.add(song);
//                    break;
//                }
//            }
//        }
//        String prevSong = PreferenceManager.getDefaultSharedPreferences(mContext).getString(PARAM_PREVIOUS_SONG, null);
//        for (int i = 0; i < out.size(); i++) {
//            if (out.get(i).Path.equals(prevSong)) {
//                mCurrentPlaySongPosition = i;
//                break;
//            }
//        }
//        mCurrentPlaySongs = out.toArray(new Song[out.size()]);
    }

    private final class TimeTimerTask extends TimerTask {
        @Override
        public void run() {
//            notifyHandler(MUSIC_PLAYER_EVENT.TIME_UPDATE);
            uiHandler.post(new PostRunnable(MUSIC_PLAYER_EVENT.TIME_UPDATE));
            try {
                mTimer.schedule(new TimeTimerTask(), TIMER_UPDATE_TIME);
            } catch (Exception e) {
            }
        }
    }

}
