package com.msoft.android.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by artyom on 7/22/14.
 */
public class MusicDataBase extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "mymusic.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_FAVOURITE = "tbl_favourite";
    public static final String KEY_ID = "_id";
    public static final String KEY_PATH = "_path";

    public static final String TABLE_RECENT_ALBUMS = "tbl_recent";
    public static final String TABLE_RECENT_SONGS = "tbl_recent";

    public static final String KEY_RECENT_ID = "_recent_id";

    private static final String CREATE_TABLE_FAVOURITE = "create table " + TABLE_FAVOURITE + "("
            + KEY_ID + " integer primary key, " + KEY_PATH + " text);";

    private static final String CREATE_TABLE_RECENT_ALBUMS = "create_table " + TABLE_RECENT_ALBUMS + "("
            + KEY_ID + " integer primary key, " + KEY_RECENT_ID + " integer);";

    private static final String CREATE_TABLE_RECENT_SONGS = "create_table " + TABLE_RECENT_SONGS + "("
            + KEY_ID + " integer primary key, " + KEY_RECENT_ID + " integer);";

    public MusicDataBase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_FAVOURITE);
        sqLiteDatabase.execSQL(CREATE_TABLE_RECENT_ALBUMS);
        sqLiteDatabase.execSQL(CREATE_TABLE_RECENT_SONGS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        sqLiteDatabase.execSQL("drop table if exists " + TABLE_FAVOURITE);
        sqLiteDatabase.execSQL("drop table if exists " + TABLE_RECENT_ALBUMS);
        sqLiteDatabase.execSQL("drop table if exists " + TABLE_RECENT_SONGS);
        onCreate(sqLiteDatabase);
    }

}
