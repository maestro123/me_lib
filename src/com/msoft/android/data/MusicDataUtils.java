package com.msoft.android.data;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import com.msoft.android.mplayer.lib.models.Album;
import com.msoft.android.mplayer.lib.models.Song;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by artyom on 7/22/14.
 */
public class MusicDataUtils {
    //
    private static volatile MusicDataUtils instance;

    public static synchronized MusicDataUtils getInstance(Context context) {
        if (instance == null)
            instance = new MusicDataUtils(context);
        return instance;
    }

    private MusicDataBase musicDataBase;
    private SQLiteDatabase mDatabase;

    MusicDataUtils(Context context) {
        musicDataBase = new MusicDataBase(context);
        mDatabase = musicDataBase.getWritableDatabase();
    }

    public void addSongToFavourite(Song song) {
        ContentValues values = new ContentValues();
        values.put(MusicDataBase.KEY_PATH, song.Path);
        mDatabase.insert(MusicDataBase.TABLE_FAVOURITE, null, values);
    }

    public void removeSongFromFavourite(Song song) {
        mDatabase.delete(MusicDataBase.TABLE_FAVOURITE, MusicDataBase.KEY_PATH + "=" + song.Path, null);
    }

    public List<String> getSongsFromFavourite() {
        List<String> paths = new ArrayList<String>();
        Cursor cursor = mDatabase.query(MusicDataBase.TABLE_FAVOURITE, null, null, null, null, null, null);
        if (cursor.getCount() > 0) {
            do {
                paths.add(cursor.getString(cursor.getColumnIndexOrThrow(MusicDataBase.KEY_PATH)));
            } while (cursor.moveToNext());
        }
        return paths;
    }

    public void addAlbumToRecent(Album album) {
        ContentValues values = new ContentValues();
        values.put(MusicDataBase.KEY_RECENT_ID, album.Id);
        mDatabase.insert(MusicDataBase.TABLE_RECENT_ALBUMS, null, values);
    }

    public void removeAlbumFromRecent(Album album) {
        mDatabase.delete(MusicDataBase.TABLE_RECENT_ALBUMS, MusicDataBase.KEY_RECENT_ID + "=" + album.Id, null);
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD_MR1)
    public static Song makeSongFromFile(File file) {
        Song song = new Song();
        MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
        metaRetriever.setDataSource(file.getPath());
        song.Album = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
        if (song.Album == null || song.Album.isEmpty()) {
            song.Album = file.getParent();
        }
        song.AlbumArtist = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUMARTIST);
        song.Artist = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
        song.Author = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_AUTHOR);
        song.Bitrate = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_BITRATE);
        song.TrackNumberCD = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_CD_TRACK_NUMBER);
        song.Compilation = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_COMPILATION);
        song.Composer = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_COMPOSER);
        song.Date = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DATE);
        song.DiscNumber = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DISC_NUMBER);
        song.Duration = Long.valueOf(metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
        song.Genre = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE);
        song.MimeType = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_MIMETYPE);
        song.Writer = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_WRITER);
        song.Year = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR);
        song.Title = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
        metaRetriever.release();
        return song;
    }


}
