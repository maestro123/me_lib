package com.msoft.android.data;

import android.os.FileObserver;

/**
 * Created by artyom on 7/22/14.
 */
public class MusicScannerObserver extends FileObserver {

    private MusicScanner mScanner = MusicScanner.getInstance();

    public MusicScannerObserver(String path) {
        super(path);
    }

    @Override
    public void onEvent(int i, String s) {
        switch (i) {
            case CREATE:
                mScanner.checkAndProcess(s, false);
                break;
            case DELETE:
                mScanner.checkAndProcess(s, true);
                break;
        }
    }

}
