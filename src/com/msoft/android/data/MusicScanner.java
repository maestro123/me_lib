package com.msoft.android.data;

import android.os.Environment;
import android.util.Log;
import com.msoft.android.mplayer.lib.models.Song;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by artyom on 7/22/14.
 */
public class MusicScanner {

    public static final String TAG = MusicScanner.class.getSimpleName();

    private volatile static MusicScanner instance;

    public static synchronized final MusicScanner getInstance() {
        MusicScanner localInstance = instance;
        if (localInstance == null) {
            synchronized (MusicScanner.class) {
                localInstance = instance;
                if (localInstance == null) {
                    localInstance = instance = new MusicScanner();
                }
            }
        }
        return localInstance;
    }

    //TODO: before start, better take list of supported formats from device
    public static final String MUSIC_FORMAT_PATTERN = "^.+?\\.(mp3)$";

    private boolean isScanStart;
    private MusicScannerObserver mObserver;
    private ArrayList<String> mPaths = new ArrayList<String>();

    public void startScan() {
        if (isScanStart) return;
        new Thread() {
            @Override
            public void run() {
                super.run();
                File rootFile = Environment.getExternalStorageDirectory();
                if (mObserver != null) {
                    mObserver.stopWatching();
                }
                mObserver = new MusicScannerObserver(rootFile.getPath());
                mObserver.startWatching();

                //TODO: collect all paths from database

                for (String path : mPaths) {
                    if (new File(path).exists()) {
                        checkAndProcess(path, true);
                    }
                }

                recursiveMusicInsert(rootFile);

                isScanStart = false;
            }
        }.start();
    }

    public void recursiveMusicInsert(File rootFile) {
        File[] files = rootFile.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    recursiveMusicInsert(file);
                } else if (!mPaths.contains(file.getPath())
                        && file.getPath().toLowerCase().matches(MUSIC_FORMAT_PATTERN)) {
                    processSongFromFile(file);
                }
            }
        }
    }

    private void processSongFromFile(File file) {
        Song song = MusicDataUtils.makeSongFromFile(file);
        if (song != null) {
            //TODO: add song to database and process listeners
            Log.e(TAG, "found song = " + song);
        }
    }

    public void checkAndProcess(String path, boolean delete) {
        if (delete) {

        }
    }

    public interface MusicScannerEventListener {

        public enum MUSIC_SCANNER_EVENT {
            SONG_ADD, SONG_DELETE
        }

        public void onEvent(MUSIC_SCANNER_EVENT event, String path);
    }

}
