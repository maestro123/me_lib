package com.msoft.android.helpers;

import android.support.v4.app.Fragment;

public abstract class MFragment extends Fragment{

	public String getPageTitle(){
		return null;
	}
	
	public void update(){}
	
	public void update(Object ob){}
	
	public void updateList(){}
	
	public void notifyList(){}
	
}
